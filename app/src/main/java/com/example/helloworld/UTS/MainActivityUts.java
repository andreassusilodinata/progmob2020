package com.example.helloworld.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.helloworld.R;

import android.os.Handler;

import android.content.Intent;

public class MainActivityUts extends AppCompatActivity {

    private int waktu_loading=4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_uts);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //setelah loading maka akan langsung berpindah ke home activity
                Intent home=new Intent(MainActivityUts.this, MainActivityUtsLogin.class);
                startActivity(home);
                finish();

            }
        },waktu_loading);
    }
}