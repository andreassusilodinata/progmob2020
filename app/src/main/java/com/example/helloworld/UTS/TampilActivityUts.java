package com.example.helloworld.UTS;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.CRUD.MainMhsActivity;
import com.example.helloworld.CRUD_Dosen.MainDsnActivity;
import com.example.helloworld.CRUD_Matkul.MainMatkulActivity;
import com.example.helloworld.R;

public class TampilActivityUts extends AppCompatActivity {
    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_uts);

        TextView tampil = (TextView)findViewById(R.id.textViewWelcome);
        ImageButton a = (ImageButton) findViewById(R.id.imageButtonLogout);
        ImageButton mhs = (ImageButton) findViewById(R.id.imageButtonMhs);
        ImageButton dsn = (ImageButton) findViewById(R.id.imageButtonDsn);
        ImageButton mk = (ImageButton) findViewById(R.id.imageButtonMatkul);

        Bundle b = getIntent().getExtras();
        String textHelp = b.getString("help_string");
        tampil.setText(textHelp);

        SharedPreferences main = TampilActivityUts.this.getSharedPreferences("main_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = main.edit();

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilActivityUts.this, MainActivityUtsLogin.class);
                startActivity(intent);
                isLogin = main.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                }else{
                    editor.putString("isLogin","0");
                }
                editor.commit();
                /*new AlertDialog.Builder(getApplication())
                        .setTitle("Apakah Kamu Ingin Keluar?")
                        .setMessage("Ingin Keluar Dari Aplikasi Ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "Anda Telah Logout", Toast.LENGTH_LONG).show();
                                //Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                                //startActivity(intent);
                            }
                        })
                        .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplication(), "Anda Tidak Jadi Keluar", Toast.LENGTH_LONG).show();
                            dialog.cancel();
                    }
                }).show();*/

            }
        });

        mhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilActivityUts.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        dsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilActivityUts.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });

        mk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilActivityUts.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });

    }
}
