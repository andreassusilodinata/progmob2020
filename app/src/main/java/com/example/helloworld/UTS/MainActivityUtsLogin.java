package com.example.helloworld.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.helloworld.CRUD.MainMhsActivity;
import com.example.helloworld.R;

public class MainActivityUtsLogin extends AppCompatActivity {

    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_uts_login);

        Button buttonLogin = (Button)findViewById(R.id.buttonLogin);

        SharedPreferences main = MainActivityUtsLogin.this.getSharedPreferences("main_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = main.edit();

        isLogin = main.getString("isLogin", "0");
        if(isLogin.equals("1")){
            buttonLogin.setText("Logout");
        }else{
            buttonLogin.setText("Login");
        }

        final EditText user = (EditText)findViewById(R.id.editTextUserName);
        final EditText pass = (EditText)findViewById(R.id.editTextPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityUtsLogin.this, TampilActivityUts.class);
                Bundle b = new Bundle();
                b.putString("help_string", user.getText().toString());
                intent.putExtras(b);

                if(user.getText().toString().length() == 0){
                    user.setError("Masukkan Username");
                }else if(pass.getText().toString().length()==0){
                    pass.setError("Masukkan Password");
                }else{
                    Toast.makeText(getApplicationContext(),"Berhasil Login", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }
                isLogin = main.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    buttonLogin.setText("Logout");
                }else{
                    editor.putString("isLogin","0");
                    buttonLogin.setText("Login");
                }
                editor.commit();
            }
        });
    }
}