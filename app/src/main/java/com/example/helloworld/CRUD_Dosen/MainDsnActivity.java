package com.example.helloworld.CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.helloworld.CRUD.HapusMahasiswaActivity;
import com.example.helloworld.CRUD.LihatMahasiswaActivity;
import com.example.helloworld.CRUD.MainMhsActivity;
import com.example.helloworld.CRUD.TambahMahasiswaActivity;
import com.example.helloworld.CRUD.UpdateMahasiswaActivity;
import com.example.helloworld.R;

public class MainDsnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dsn);

        Button btnLihat = (Button)findViewById(R.id.btnLihat);
        Button btnTambah = (Button)findViewById(R.id.btnTambah);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdate);

        btnLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, LihatDosenActivity.class);
                startActivity(intent);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, TambahDosenActivity.class);
                startActivity(intent);
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, DeleteDosenActivity.class);
                startActivity(intent);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDsnActivity.this, UpdateDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}