package com.example.helloworld.CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.helloworld.CRUD_Dosen.DeleteDosenActivity;
import com.example.helloworld.CRUD_Dosen.LihatDosenActivity;
import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteDosenActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_dosen);

        EditText edNidn = (EditText)findViewById(R.id.editNidn);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        pd = new ProgressDialog(DeleteDosenActivity.this);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dsn(
                        edNidn.getText().toString(),
                        "72180187"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DeleteDosenActivity.this,"Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DeleteDosenActivity.this, LihatDosenActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DeleteDosenActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}