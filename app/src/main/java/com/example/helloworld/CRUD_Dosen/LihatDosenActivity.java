package com.example.helloworld.CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.helloworld.Adapter.DosenCRUDRecycleAdapter;
import com.example.helloworld.CRUD_Dosen.LihatDosenActivity;
import com.example.helloworld.Model.Dosen;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatDosenActivity extends AppCompatActivity {

    RecyclerView rvDsn;
    DosenCRUDRecycleAdapter dsnAdapter;
    List<Dosen> dosenList;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_dosen);
        rvDsn = (RecyclerView)findViewById(R.id.rvGetDsnAll);

        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180187");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapter = new DosenCRUDRecycleAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatDosenActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(dsnAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText( LihatDosenActivity.this,"Error", Toast.LENGTH_LONG).show();

            }
        });
    }
}