package com.example.helloworld.CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.helloworld.CRUD_Dosen.LihatDosenActivity;
import com.example.helloworld.CRUD_Dosen.TambahDosenActivity;
import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahDosenActivity extends AppCompatActivity {

    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dosen);

        EditText edNama = (EditText) findViewById(R.id.editTextNama);
        EditText edNidn = (EditText) findViewById(R.id.editTextNidn);
        EditText edAlamat = (EditText) findViewById(R.id.editTextAlamat);
        EditText edEmail = (EditText) findViewById(R.id.editTextEmail);
        EditText edGelar = (EditText) findViewById(R.id.editTextGelar);
        Button btnSimpan = (Button) findViewById(R.id.buttonSimpanDsn);

        pd = new ProgressDialog(this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pd.setTitle("Mohon bersabar");
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        edNama.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        edGelar.getText().toString(),
                        "Kosong",
                        "72180187"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this, "Data Berhasil Disimpan", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TambahDosenActivity.this, LihatDosenActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(TambahDosenActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}