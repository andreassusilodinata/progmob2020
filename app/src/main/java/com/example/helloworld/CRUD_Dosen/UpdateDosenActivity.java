package com.example.helloworld.CRUD_Dosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.helloworld.CRUD.MainMhsActivity;
import com.example.helloworld.CRUD.UpdateMahasiswaActivity;
import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDosenActivity extends AppCompatActivity {

    EditText editTextNidnCari, editTextNama, editTextNidn, editTextAlamat, editTextEmail, editTextGelar;
    Button btnUbah;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);

        editTextNidnCari = (EditText)findViewById(R.id.editTextNidnCari);
        editTextNama = (EditText)findViewById(R.id.editTextNamaBaru);
        editTextNidn = (EditText)findViewById(R.id.editTextNidnBaru);
        editTextAlamat = (EditText)findViewById(R.id.editTextAlamatBaru);
        editTextEmail = (EditText)findViewById(R.id.editTextEmailBaru);
        editTextGelar = (EditText)findViewById(R.id.editTextGelarBaru);
        btnUbah = (Button)findViewById(R.id.buttonUbah);

        pd = new ProgressDialog(UpdateDosenActivity.this);

        Intent data = getIntent();
        editTextNidnCari.setText(data.getStringExtra("nidn"));
        editTextNama.setText(data.getStringExtra("nama"));
        editTextNidn.setText(data.getStringExtra("nidn"));
        editTextAlamat.setText(data.getStringExtra("alamat"));
        editTextEmail.setText(data.getStringExtra("email"));
        editTextGelar.setText(data.getStringExtra("gelar"));

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();
                GetDataService service = RetrofitClientInstance. getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dsn(
                        editTextNama.getText().toString(),
                        editTextNidn.getText().toString(),
                        editTextAlamat.getText().toString(),
                        editTextEmail.getText().toString(),
                        editTextGelar.getText().toString(),
                        "Fotonya Diubah",
                        "72180187",
                        editTextNidnCari.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateDosenActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });
    }
}