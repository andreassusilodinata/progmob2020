package com.example.helloworld.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworld.Model.Dosen;
import com.example.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecycleAdapter extends RecyclerView.Adapter<DosenCRUDRecycleAdapter.ViewHolder> {
        private Context context;
        private List<Dosen> dosenList;

        public DosenCRUDRecycleAdapter(Context context) {
            this.context = context;
            dosenList = new ArrayList<>();
        }

        public DosenCRUDRecycleAdapter(List<Dosen> dosenList) {
            this.dosenList = dosenList;
        }

        public List<Dosen> getDosenList() {
            return dosenList;
        }

        public void setDosenList(List<Dosen> dosenList) {
            this.dosenList = dosenList;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public DosenCRUDRecycleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview1, parent, false);
            return new DosenCRUDRecycleAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull DosenCRUDRecycleAdapter.ViewHolder holder, int position) {
            Dosen d = dosenList.get(position);

            holder.tvNama.setText(d.getNama());
            holder.tvNidn.setText(d.getNidn());
            //holder.tvNoTelp.setText((m.getNotelp()));
            holder.tvAlamat.setText(d.getAlamat());
            holder.tvEmail.setText(d.getEmail());
            holder.tvGelar.setText(d.getGelar());
        }

        @Override
        public int getItemCount() {
            return dosenList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tvNama, tvNidn, tvNoTelp, tvAlamat, tvEmail, tvGelar;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                tvNama = itemView.findViewById(R.id.tvNama);
                tvNidn = itemView.findViewById(R.id.tvNidn);
                //tvNoTelp = itemView.findViewById(R.id.txtNoTelp);
                tvAlamat = itemView.findViewById(R.id.tvAlamat);
                tvEmail = itemView.findViewById(R.id.tvEmail);
                tvGelar = itemView.findViewById(R.id.tvGelar);
            }
        }
}
