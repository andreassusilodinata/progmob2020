package com.example.helloworld.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.helloworld.Adapter.MahasiswaRecyclerAdapter;
import com.example.helloworld.Model.Mahasiswa;
import com.example.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);

        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //Datanya
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();
        //generate data mahasiswa
        /*Mahasiswa m1 = new Mahasiswa("72180187", "Andreas", "0878000000001");
        Mahasiswa m2 = new Mahasiswa("72180188", "Susilo", "087800000002");
        Mahasiswa m3 = new Mahasiswa("72180189", "Dinata", "087800000003");
        Mahasiswa m4 = new Mahasiswa("72180190", "Baocah", "087800000004");
        Mahasiswa m5 = new Mahasiswa("72180191", "Imut", "087800000005");
        Mahasiswa m6 = new Mahasiswa("72180192", "Lucu", "087800000006");
        Mahasiswa m7 = new Mahasiswa("72180193", "Mengemaskan", "087800000007");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        mahasiswaList.add(m6);
        mahasiswaList.add(m7);*/

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }
}