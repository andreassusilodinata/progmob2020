package com.example.helloworld.CRUD_Matkul;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.helloworld.CRUD_Dosen.MainDsnActivity;
import com.example.helloworld.CRUD_Dosen.UpdateDosenActivity;
import com.example.helloworld.CRUD_Matkul.UpdateMatkulActivity;
import com.example.helloworld.Model.DefaultResult;
import com.example.helloworld.Network.GetDataService;
import com.example.helloworld.Network.RetrofitClientInstance;
import com.example.helloworld.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMatkulActivity extends AppCompatActivity {

    EditText editTextNama, editTextCari, editTextKode, editTextHari, editTextSesi, editTextSks;
    Button btnUbah;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_matkul);

        editTextCari = (EditText)findViewById(R.id.editTextKodeCari);
        editTextNama = (EditText)findViewById(R.id.editTextNamaBaru);
        editTextKode = (EditText)findViewById(R.id.editTextKodeBaru);
        editTextHari = (EditText)findViewById(R.id.editTextHariBaru);
        editTextSesi = (EditText)findViewById(R.id.editTextSesiBaru);
        editTextSks = (EditText)findViewById(R.id.editTextSksBaru);
        btnUbah = (Button)findViewById(R.id.buttonUbah);

        pd = new ProgressDialog(UpdateMatkulActivity.this);

        Intent data = getIntent();
        editTextCari.setText(data.getStringExtra("cari"));
        editTextNama.setText(data.getStringExtra("nama"));
        editTextKode.setText(data.getStringExtra("kode"));
        editTextHari.setText(data.getStringExtra("hari"));
        editTextSesi.setText(data.getStringExtra("sesi"));
        editTextSks.setText(data.getStringExtra("sks"));

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();
                GetDataService service = RetrofitClientInstance. getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        editTextNama.getText().toString(),
                        editTextKode.getText().toString(),
                        Integer.parseInt(editTextHari.getText().toString()),
                        Integer.parseInt(editTextSesi.getText().toString()),
                        Integer.parseInt(editTextSks.getText().toString()),
                        editTextCari.getText().toString(),
                        "72180187"

                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateMatkulActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}